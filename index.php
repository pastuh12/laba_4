<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

  $messages = '';
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages = 'Спасибо, результаты отправлены в базу данных.';
  }
  if (!empty($_COOKIE['notsave'])) {
    setcookie('notsave', '', 100000);
    $messages = 'Ошибка отправления в базу данных.';
  }

  $errors = array();
  // Ошибка имени, если пустая то записываем пустую строку, иначе ее значение из куки
  // Аналогично со всеми остальными
  $errors['fio'] = empty($_COOKIE['fio_error']) ? '' : $_COOKIE['fio_error'];
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['talents'] = !empty($_COOKIE['telents_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);

  // name error print
  if ($errors['name'] == 'null') {
    setcookie('name_error', '', 100000);
    $messages[] = '<div>Заполните имя.</div>';
  }
  else if ($errors['name'] == 'incorrect') {
      setcookie('name_error', '', 100000);
      $messages[] = '<div>Недопустимые символы. Введите имя заново.</div>';
  }

  // email error print
  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages[] = '<div>Заполните почту.</div>';
  }

  // powers error print
  if ($errors['powers']) {
    setcookie('powers_error', '', 100000);
    $messages[] = '<div>Выберите хотя бы одну сверхспособность.</div>';
  }

  if ($errors['bio']) {
    setcookie('bio_error', '', 100000);
    $messages[] = '<div>Напишите что-нибудь о себе.</div>';
  }

  if ($errors['check']) {
    setcookie('check_error', '', 100000);
    $messages[] = '<div>Вы не можете отправить форму не согласившись с контрактом.</div>';
  }


